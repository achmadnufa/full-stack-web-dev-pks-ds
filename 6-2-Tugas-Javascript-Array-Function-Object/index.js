//Jawaban soal 1
console.log("\n\nJawaban soal 1")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var txt=""
var daftarHewan2 = daftarHewan.sort()
daftarHewan2.forEach(urut)
function urut(value) {
    return txt += value + "\n" 
}

console.log(txt)


//Jawaban soal 2
console.log("\n\nJawaban soal 2")
function introduce(item){
    return "Nama saya "+item.name+", umur saya "+item.age+" tahun, alamat saya di "+item.address+", dan saya punya hobby yaitu "+item.hobby
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


//Jawaban soal 3
console.log("\n\nJawaban soal 3")
function hitung_huruf_vokal(huruf) {
    var a = huruf.match(/[aeiou]/gi).length;
    return a;
  }

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2


//Jawaban soal 4
console.log("\n\nJawaban soal 4")
function hitung(angka1){
    var angka2 = (angka1*2)-2
    return angka2
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8