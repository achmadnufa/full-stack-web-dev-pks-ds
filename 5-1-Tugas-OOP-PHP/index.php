<?php
trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;



    public function atraksi(){
       
        echo "<br>".$this->nama." sedang ".$this->keahlian."<br>";
    }

    public function getInfoHewan(){
       
        echo "nama : ".$this->nama."<br>";
        echo "darah : ".$this->darah."<br>";
        echo "jumlah kaki : ".$this->jumlahKaki."<br>";
        echo "keahlian : ".$this->keahlian."<br>";
        echo "attack power : ".$this->attackPower."<br>";
        echo "defence power : ".$this->defencePower."<br>";
        echo "<br>";
    }

}

trait Fight{
    public $attackPower;
    public $defencePower;

    public function serang($lawan){
        
        echo "<br>".$this->nama." sedang menyerang ".$lawan->nama;
        $lawan->diserang($this);

    }

    public function diserang($penyerang){
        echo "<br>".$this->nama." sedang diserang ".$penyerang->nama; 
        $this->darah= $this->darah-(($penyerang->attackPower)/($this->defencePower));     
        echo "<br> darah ".$this->nama." sekarang = ". $this->darah;
        echo "<br><br>";
    }
}

class Elang{
    use Hewan,Fight;

    public function __construct($nama){
        $this->setName($nama);
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }
    
    public function setName($nama){
        $this->nama=$nama;
    }
    
}

class Harimau{
    use Hewan,Fight;

    public function __construct($nama){
        $this->setName($nama);
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
    public function setName($nama){
        $this->nama=$nama;
    }


}

$elang = new Elang("elang_1");
$elang->getInfoHewan();


$harimau = new Harimau("harimau_1");
$harimau->getInfoHewan();

$elang->atraksi();
$harimau->atraksi();
$elang->serang($harimau );
$harimau->atraksi();
$elang->serang($harimau );
$harimau->serang($elang );
$elang->diserang($harimau );
$elang->serang($harimau );
$harimau->atraksi();
$harimau->serang($elang );
$harimau->serang($elang );

?>
