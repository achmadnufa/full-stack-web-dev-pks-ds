<?php

namespace App;

use App\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table='users';
    protected $fillable=['username','email','name','role_id','password','email_verified_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = 'false';

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
                if(empty($model->{$model->getkeyName()})){
                    $model->{$model->getKeyName()} = Str::uuid();
                }

               // $model->role_id = Role::where('name','author')->first()->id;
        });

    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function otpCode(){
        return $this->hasOne('App\OtpCode','user_id');
    }

    public function comment(){
        return $this->hasMany('App\Comment');
    }

    public function post(){
        return $this->hasMany('App\Post');
    }
    


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
